# %% 
import numpy as np
import matplotlib.pyplot as plt
import uncertainties
from uncertainties import ufloat
from uncertainties.unumpy import uarray
import pandas as pd

# %%
def table(arrays, names, colstyle="math", full=False, caption="", label=""):
    concarray = np.array([*arrays])
    concarray = concarray.T
#     print(*args)
#     print(concarray)
    d = {}
    for array, name in zip(arrays, names):
        d[name]=array
    k1 = pd.DataFrame(d)

    # sets the colum style (math mode or text)
    col = "|"
    if colstyle=="text":
        for i in range(len(arrays)):
            col+="c|"
    else: # math
        for i in range(len(arrays)):
            col+="C|"

    # specifies output (caption and label)
    if full==True:
        print(k1.to_latex(index=False, column_format=col, caption=caption, label=label, escape=False))
    else:
        print(k1.to_latex(index=False, column_format=col, escape=False))


# %% print values with significant digits
def pprint(values, dig=3):
    for val in values:
        style = "{:."+str(dig)+"u}"
        print(style.format(val))

# %% 

# arr = unp.uarray([1,2,3], [0.11111111, 0.22222222, 0.34567])
# barr = unp.uarray([7,8,9], [0.000002, 0.88888, 0.99])
# print("{:.1u}".format(arr[0]))

# table([arr, barr], [r"Arr [\%]", r"Barr [\frac{kg}{m}]"], colstyle="text", full=True, caption="Test", label="testtable")

# %% task 1: method 1 in cm
# Lens 2
# error, weil grid immer noch scharf in diesem bereich:
dl = 0.5 # error of length on scale when focussing assumed the same for all measurements but actually dependent on position of the element
ds = 0.05

a21 = ufloat(112.7,ds)-ufloat(94.4, dl)
b21 = ufloat(94.4,dl)-ufloat(16.6, ds)
a22 = ufloat(100.0,ds)-ufloat(81.3, dl)
b22 = ufloat(81.3,dl)-ufloat(10.0, ds)

# Lens 1
# error:
a11 = ufloat(100.0,ds)-ufloat(92.0, dl)
b11 = ufloat(92.0,dl)-ufloat(10.0, ds)
a12 = ufloat(120.0,ds)-ufloat(112.7, dl)
b12 = ufloat(112.7,dl)-ufloat(10.0, ds)

# values=[a11,b11,a12,b12]
values=[a21,b21,a22,b22]
pprint(values, dig=1)

# %% 
def get_f(a,b):
    f = 1/(1/a+1/b)
    return f

# %%

f11 = get_f(a11, b11)
f12 = get_f(a12, b12)
f21 = get_f(a21, b21)
f22 = get_f(a22, b22)

values=[f11,f12,f21,f22]
pprint(values, dig=1)

# table([["\text{run 1}", "\text{run 2}"], [a11, a12], [b11, b12], [f11, f12]], ["\text{lens 1}", "a [cm]", "b [cm]", "f [cm]"], full=True, caption="Measurement results for task 1 a) for lens 1.", label="tab:lens1")

# table([["\text{run 1}", "\text{run 2}"], [a21, a22], [b21, b22], [f21, f22]], ["\text{lens 2}", "a [cm]", "b [cm]", "f [cm]"], full=True, caption="Measurement results for task 1 a) for lens 2.", label="tab:lens2")

# %% task 1: method 2 (Bessel) in cm
# Lens 2:
pos2_links = ufloat(29.5,dl)-ufloat(10.0, ds)
pos2_rechts = ufloat( 70.0 ,dl)-ufloat( 10.0, ds)
e2 = pos2_rechts-pos2_links
d2 = ufloat(90.0,ds)-ufloat(10.0, ds)

# Lens 1:
pos1_links = ufloat(20.7,dl)-ufloat(10.0, ds)
pos1_rechts = ufloat(37.0,dl)-ufloat(10.0, ds)
e1 = pos1_rechts-pos1_links
d1 = ufloat(48.4,ds)-ufloat(10.0, ds)

values=[e1,d1,e2,d2]
pprint(values, dig=2)
# %% 
def get_f_bessel(d,e):
    f = (d**2-e**2)/(4*d)
    return f

f1_b = get_f_bessel(d1,e1)
f2_b = get_f_bessel(d2,e2)

vals = [f1_b, f2_b]
pprint(vals, dig=1)

# %% task 2: in cm

# Lens 1+div

asys11 = ufloat(100.0,ds)-ufloat(91.2, dl)
bsys11 = ufloat(91.2, dl)-ufloat(10.0,ds)
asys12 = ufloat(125.0,ds)-ufloat(116.4, dl)
bsys12 = ufloat(116.4,dl)-ufloat(10.0, ds)

# Lens 2+div

asys21 = ufloat(100.0,ds)-ufloat(70.5, dl)
bsys21 = ufloat(70.5, dl)-ufloat(10.0,ds)
asys22 = ufloat(125.0,ds)-ufloat(99.9, dl)
bsys22 = ufloat(99.9,dl)-ufloat(10.0, ds)

# %% 
def get_f_div(f_ref, fsys):
    f_div = 1/(1/fsys-1/f_ref)
    return f_div

#%% 
fsys11 = get_f(asys11,bsys11)
fsys12 = get_f(asys12,bsys12)
fsys21 = get_f(asys21,bsys21)
fsys22 = get_f(asys22,bsys22)

f2 = np.mean([f21, f22, f2_b])
f1 = np.mean([f11, f12, f1_b]) # for later

print("means f1, f2:")
vals = [f1,f2]
pprint(vals, dig=1)
print("system focal lengths:")
vals = [fsys11,fsys12, fsys21, fsys22]
pprint(vals, dig=1)

fdiv11 = get_f_div(f1, fsys11)
fdiv12 = get_f_div(f1, fsys12)
fdiv21 = get_f_div(f2, fsys21)
fdiv22 = get_f_div(f2, fsys22)

print("div focal lengths:")
vals = [fdiv11,fdiv12, fdiv21, fdiv22]
pprint(vals, dig=1)


# table([[r"\txt{lens 1}", r"\txt{lens 2}"], [fsys11, fsys21], [fdiv11, fdiv21], [fsys12, fsys22], [fdiv12, fdiv22], [np.mean([fdiv11, fdiv12]), np.mean([fdiv21, fdiv22])]], ["", "f_{sys} for run 1", "f_{div} for run 1", "f_{sys} for run 2", "f_{div} for run 2", "\mean{f_{div}}"], full=True, caption="Measurement results for the diverging lens usign lens 1 and 2 in two runs each.", label="tab:div")
#%% NOTE
# the error blows up for the first lens because  f1-fsys11 ~= -0.6+/-0.5 which results in a huge relative error

fdivmean1 = np.mean([fdiv11, fdiv12])
fdivmean2 = np.mean([fdiv21, fdiv22])


# %% task 3: in cm
# grid 1 + L2:
dgprime=0.05 # error on wall per square
# measure 10 squares for gprime for smaller error

gprime1 = ufloat(22.4, dgprime)/10.0
a31 = ufloat(90.0,ds)-ufloat(74.6, dl)
tisch = ufloat(1757,2)*1e-1
b31 = tisch + ufloat(74.6, dl)
v1 = b31/a31
g1 = gprime1/v1

# vals=[a31, b31, v1, gprime1, g1]
# pprint(vals, dig=1)
table([[r"\text{grid 1}", r"\text{grid 2}"], [a31, a32], [b31, b32], [v1, v2], [gprime1, gprime2], [g1, g2]], ["\text{grid}", "a [cm]", "b [cm]", "v", "g^\prime [cm]", "g [cm]"], full=True, caption="Measurement results for the grating constants of two grids.", label="tab:grids")

# grid 2 + L2:
# measure 16 squares
gprime2 = ufloat(23.65, dgprime)/16.0
a32 = ufloat(90.0,ds)-ufloat(74.7, dl)
b32 = tisch + ufloat(74.7, dl)
v2 = b32/a32
g2 = gprime2/v2

print("----------------------")
# vals=[a32, b32, v2, gprime2, g2]
# pprint(vals, dig=1)
# %% task 4: 
# no error in lambda in cm
lam = ufloat(525, 0)*1e-9*100

# source of error: setting slit-lens-dist to focal length

# grid 1 + L 2
dist_lens_slit = ufloat(f2.n, 0.05+f2.s)

dprime1 = ufloat(0.12, dgprime)
a41 = ufloat(134.4,ds)-ufloat(118.8, dl)
b41 = tisch + ufloat(118.8, dl)
v41 = b41/a41
d1exp = dprime1/v41
d1calc = f2*lam/g1
# print(d1exp, d1calc)
# print(d1exp.std_score(d1calc))

vals = [a41, b41, dprime1, d1exp, d1calc]
pprint(vals, dig=3)

# grid 2 + L 2
# move object close to laser for high intensity on wall
dist_lens_slit = ufloat(f2.n, 0.05+f2.s)

dprime2 = ufloat(0.2, dgprime)
a42 = ufloat(124.3,ds)-ufloat(108.7, dl)
b42 = tisch + ufloat(108.7, dl)
v42 = b42/a42
d2exp = dprime2/v42
d2calc = f2*lam/g2
# print(d2exp, d2calc)
# print(d2exp.std_score(d2calc))

print("------------------")
vals =  [a42, b42, dprime2, d2exp, d2calc]
pprint(vals, dig=3)

# table([[r"\text{grid 1}", r"\text{grid 2}"], [a41, a42], [b41, b42], [dprime1, dprime2], [d1exp, d2exp], [d1calc, d2calc]], ["\text{grid}", "a [cm]", "b [cm]", "d^\prime [cm]", "d_{exp}", "d_calc"], full=True, caption="Measurement results for task 4.", label="tab:abbe")
# %%

divd1 = abs(d1exp-d1calc)/d1calc
divd2 = abs(d2exp-d2calc)/d2calc
pprint([divd1,divd2], dig=5)